/*
 * @Author: zhanglianchang
 * @Date: 2020-08-01 14:39:32
 * @Last Modified by: taoshiwei
 * @Last Modified time: 2019-04-29 14:56:00
 *
 * 目录
 */

import { createAPI } from '@/utils/request'

// 目录列表
export const list = data => createAPI('/directorys', 'get', data)

// 目录简单列表
export const simple = data => createAPI('/directorys', 'post')

// 目录详情
export const detail = id => createAPI(`/directorys/${id}`, 'get')

// 目录添加
export const add = data => createAPI('/directorys', 'post', data)

// 目录修改
export const updateById = data =>
  createAPI(`/directorys/${data.id}`, 'put', data)

// 目录删除
export const remove = id => createAPI(`/directorys/${id}`, 'delete')

// 目录状态
export const changeState = data =>
  createAPI(`/directorys/${data.id}/${data.state}`, 'post')
