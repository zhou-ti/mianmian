/*
 * @Author: zhanglianchang
 * @Date: 2020-08-01 14:39:32
 * @Last Modified by: taoshiwei
 * @Last Modified time: 2019-04-29 14:49:44
 *
 * 题库有关的接口：自己写吧~~  已经被张老师删了 ^_^
 */
import { createAPI } from '@/utils/request'

// 题库-组题列表
export const randomsList = data => createAPI('/questions/randoms/', 'get', data)
// 删除数据
export const deldomsList = data =>
  createAPI(`/questions/randoms/${data.id}`, 'delete', data)
// 基础题库列表
export const getQuestionsForm = data => createAPI('/questions', 'get', data)
// 题库-组题列表
export const getSubjects = data => createAPI('/subjects/simple', 'get', data)
// 题库-组题列表
export const getdirectorys = data => createAPI('/directorys', 'get', data)
// 题库-组题列表
export const getcompanys = data => createAPI('/companys', 'get', data)
// 题库-组题列表
export const gettags = data => createAPI('/tags', 'get', data)
// 题库-组题列表
export const getquestions = data =>
  createAPI(`/questions/${data.id}`, 'get', data)
// 题库-组题列表
export const postquestions = data => createAPI('/questions', 'post', data)
export const updatequestions = data => createAPI(`/questions/${data.id}`, 'put', data)

// 回显测试链接
export const textApp = data => createAPI(`/questions/${data.id}`, 'get', data)
export const remove = data => createAPI(`/questions/${data.id}`, 'delete', data)
export const add = data =>
  createAPI(`/questions/choice/${data.id}/${data.choiceState}`, 'patch', data)
export const addPublishState = data =>
  createAPI(`/questions/choice/${data.id}/${data.publishState}`, 'post', data)
export const getChoiceForm = data => createAPI('/questions/choice', 'get', data)
export const getChk = data =>
  createAPI(`/questions/check/${data.id}`, 'post', data)
export const getFullName = data => createAPI('/users/simple', 'get', data)
// 目录详情
export const tagsDetail = data => createAPI(`/tags/${data.id}`, 'get')
export const directorysDetail = data => createAPI(`/directorys/${data.id}`, 'get')
