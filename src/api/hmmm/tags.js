/*
 * @Author: zhanglianchang
 * @Date: 2020-08-01 14:39:32
 * @Last Modified by: taoshiwei
 * @Last Modified time: 2019-04-29 14:56:00
 *
 * 目录
 */

import { createAPI } from '@/utils/request'

// 目录列表
export const list = data => createAPI('/tags', 'get', data)

// 目录简单列表
export const simple = data => createAPI('/tags', 'post')

// 目录详情
export const detail = id => createAPI(`/tags/${id}`, 'get')

// 目录添加
export const add = data => createAPI('/tags', 'post', data)

// 目录修改
export const updateById = data => createAPI(`/tags/${data.id}`, 'put', data)

// 目录删除
export const remove = id => createAPI(`/tags/${id}`, 'delete')

// 目录状态
export const changeState = data =>
  createAPI(`/tags/${data.id}/${data.state}`, 'post')
