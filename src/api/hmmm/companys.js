import { createAPI } from '@/utils/request'

export const companyslist = data => createAPI('/companys', 'get', data)
export const add = data => createAPI('/companys', 'post', data)
export const update = data => createAPI(`/companys/${data.id}`, 'put', data)
export const remove = id => createAPI(`/companys/${id}`, 'delete')
export const detail = id => createAPI(`/companys/${id}`, 'get')
export const disabled = ({ id, state }) =>
  createAPI(`/companys/${id}/${state}`, 'post')
